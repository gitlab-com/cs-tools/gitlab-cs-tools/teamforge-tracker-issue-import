# TeamForge Tracker Issue Import

This project provides a script to import TeamForge Tracker Artifacts and Items into GitLab Project Issues.

It is based on multiple tables exported from the TeamForge database.

It features:

* mapping of TeamForge to GitLab projects
* automatic mapping of users based on email-addresses
* creation of labels for tracker name, priority and creator
* preservation of issue state (open / closed)

# Setup

## Source data

Export all rows and columns from TeamForge tables:

* `sfuser`
* `relationship`
* `artifact`
* `item`
* `folder`
* `project`

and store them in a `sourcetables` folder.

Format: `csv`, Encoding: `UTF-8` , delimiter: `;`

## Project mapping

In order to map TeamForge projects to GitLab projects and import the issues in the correct place, a mapping file is needed. Only isses with mapped projects will be imported.

Create `project_mapping.csv` in a `sourcetables` folder.

Format: `csv`, Encoding: `UTF-8` , delimiter: `;`

Columns: `teamforge_id;gitlab_id`

## Usage

Once all data has been collected, start the script to import issues:

`pip3 install python-gitlab`

`python import-teamforge.py $GITLAB_URL $GITLAB_TOKEN $SOURCE_FOLDER --execute`

Parameters:
* `$GITLAB_URL`: The URL of the GitLab instance
* `$GITLAB_TOKEN`: An admin API token with api scope. Note that this is a very powerful token. Recommendation is to set expiry date as low as possible to do the import. Delete the token when you are done.
* `$SOURCE_FOLDER`: The folder where the source data tables and the project mapping is stored
* `--execute`: Actually import the issues, default is False, i.e. dryrun mode (only show what would be imported where)
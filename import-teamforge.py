import csv
import argparse
import gitlab

class Importer():

    gl = None
    token = None
    t_project = {}
    t_folder = {}
    t_item = {}
    t_artifact = {}
    t_relationship = {}
    t_user = {}
    project_map = {}
    gitlab_projects = {}
    gitlab_user_dict = {}

    def __init__(self, args):
        gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
        self.token = args.token
        self.gl = gitlab.Gitlab(gitlab_url, private_token=self.token)
        projectfile = args.datafolder + "/ctf-project.csv"
        folderfile = args.datafolder + "/ctf-folder.csv"
        itemfile = args.datafolder + "/ctf-item.csv"
        artifactfile = args.datafolder + "/ctf-artifact.csv"
        relationshipfile = args.datafolder + "/ctf-relationship.csv"
        userfile = args.datafolder + "/ctf-sfuser.csv"
        mappingfile = args.datafolder + "/project_mapping.csv"
        #might not need project table as I got the id for mapping already from folder table
        self.t_project = self.read_csv(projectfile, "id")
        self.t_folder = self.read_csv(folderfile, "id")
        self.t_item = self.read_csv(itemfile, "id")
        self.t_artifact = self.read_csv(artifactfile, "id")
        self.t_relationship = self.read_csv(relationshipfile, "id")
        self.t_user = self.read_csv(userfile, "id")
        self.project_map = self.read_csv(mappingfile, "teamforge_id")
        print("Crawling GitLab projects in project map.")
        for gitlab_project_id in self.project_map.values():
            if gitlab_project_id["gitlab_id"]:
                try:
                    gitlab_p = self.gl.projects.get(gitlab_project_id["gitlab_id"])
                    self.gitlab_projects[gitlab_project_id["gitlab_id"]] = gitlab_p
                except:
                    print("Error: Could not get GitLab project: %s " % gitlab_project_id["gitlab_id"])


    def read_csv(self, file, idcolumn):
        result = None
        with open(file, "r") as csvfile:
            reader = csv.DictReader(csvfile, delimiter = ";")
            result = {}
            for row in reader:
                key = row.pop(idcolumn)
                if key in result:
                    # implement your duplicate row handling here
                    pass
                result[key] = row
            return result

    def compile_issue_data(self):
        #issues are artifacts, stored as items in trackers assigned to projects
        #trackers should become labels of issues
        #artifact metadata should be labels or stored in description
        #users associated with artifacts are assigned via relationships
        #see page 30 here for an example query: https://www.open.collab.net/community/ctf/teamforge_6.1_eval_guide.pdf
        issues = []
        for art_id, artifact in self.t_artifact.items():
            issue = {}
            item = self.t_item[art_id]
            issue["title"] = art_id + ": " + item["title"]
            #version;date_created;date_last_modified;is_deleted;type_id;folder_id
            issue["date_created"] = item["date_created"]
            issue["date_modified"] = item["date_last_modified"]
            folder = self.t_folder[item["folder_id"]]
            issue["labels"] = [folder["title"]]
            issue["labels"].append("Teamforge")
            project = self.project_map[folder["project_id"]]
            if project["gitlab_id"]:
                issue["project"] = project["gitlab_id"]
            else:
                print("ERROR: No GitLab project found for project: %s %s. Issue will not be migrated." % (folder["project_id"], self.t_project[folder["project_id"]]["title"]))
                continue
            #artifact fields
            issue["description"] = artifact["description"]
            #Artifact: lots of field values to unpack for status etc
            issue["labels"].append("priority::" + artifact["priority"])
            issue["weight"] = artifact["estimated_effort"]
            #Artifact: actual_effort;remaining_effort;autosumming;points
            issue["close_date"] = artifact["close_date"]
            if issue["close_date"]:
                issue["status"] = "closed"
            issue["creator"], issue["assignee"] = self.get_creator(art_id)
            issues.append(issue)
        return issues

    def get_creator(self, artifact_id):
        #get the user that created it
        #I'm guessing we want artifactassignment relationships where created_by_id == origin_id -> a user assigns an artifact to themselves
        #creator may be different from assignee: use created_by_id for creator and origin_id for assignee 
        #needs more investigation
        creator_email = ""
        found_relationship = False
        found_creator = False
        for relationship in self.t_relationship.values():
            if relationship["target_id"] == artifact_id:
                found_relationship = True
                if relationship["relationship_type_name"] == "ArtifactAssignment":        
                    creator = relationship["created_by_id"]
                    assignee = relationship["origin_id"]
                    found_creator = True
                    if creator in self.t_user:
                        creator_email = self.t_user[creator]["email"]
                    else:
                        print("Warning: %s not in sfuser table. Can not map user." % creator)
                    if assignee in self.t_user:
                        assignee_email = self.t_user[assignee]["email"]
                    else:
                        print("Warning: %s not in sfuser table. Can not map user." % creator)
        if not found_relationship:
            print("Warning: No relationship found for artifact %s." % artifact_id)
        if not found_creator:
            print("Warning: No creator found in relationship for artifact %s." % artifact_id)
        return creator_email, assignee_email

    def import_issues(self, issue_list, do_import):
        for issue in issue_list:
            project = self.gitlab_projects[issue["project"]]
            #can currently not change creator
            creator_user = self.find_user(issue["creator"])
            assignee_user = self.find_user(issue["assignee"])
            if not do_import:
                print("Issue to create: %s in project %s" % (str(issue), project.attributes["path_with_namespace"]))
            else:

                if issue["creator"]:
                    issue["labels"].append("creator::" + issue["creator"])

                labels = ",".join(issue["labels"])

                if creator_user and not issue["close_date"]:
                    #only ping people on open issues
                    issue["description"] = issue["description"] + "\n\n**Creator: @%s**" % creator_user.attributes["username"]
                    #could check if they are members of the project as well

                if issue["close_date"]:
                    issue["description"] = issue["description"]+ "\n\n**Close date: %s**" % issue["close_date"]

                issue_attributes = {"title": issue["title"], "description": issue["description"],
                    "labels": labels, "created_at": issue["date_created"], "weight": issue["weight"]}

                if assignee_user:
                    issue_attributes["assignee_ids"] = [ assignee_user.attributes["id"] ]

                print("Creating issue: %s in project %s" % (str(issue_attributes), project.attributes["path_with_namespace"]))

                gitlab_issue = project.issues.create(issue_attributes)

                if issue["close_date"]:
                    gitlab_issue.state_event = 'close'
                    gitlab_issue.save()

    def find_user(self, email):
        if not email:
            return None
        if email in self.gitlab_user_dict:
            return self.gitlab_user_dict[email]
        else:
            users = self.gl.users.list(search=email)
            if users:
                self.gitlab_user_dict[email] = users[0]
                return users[0]
            else:
                print("Warning: no GitLab user found for %s " % email)
        return None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Import teamforge issues')
    parser.add_argument('gitlab', help='GitLab URL')
    parser.add_argument('token', help='API token able to write on all projects (probably admin)')
    parser.add_argument('datafolder', help='Folder containing the data files')
    parser.add_argument('--execute', help='Actually trigger the import, default is false, i.e. dryrun mode', action="store_true")
    args = parser.parse_args()
    if args.execute:
        print("Execution mode, issues will be created")
    else:
        print("Dryrun mode, will output a preview of what will be imported")
    
    importer = Importer(args)
    issues = importer.compile_issue_data()
    importer.import_issues(issues, args.execute)
